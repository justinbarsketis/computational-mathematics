# Computational Mathematics

In this repo I will be putting code that helps compute mathematics, and also for mathematical modeling.

## Installation

To be determined, I will most liklely use python for all of these projects.

## Usage

This code is to be used for learning purposes only or reference material.

## Support

Email justinbarsketis@gmail.com

## Roadmap

- Micro Economics
- Probability and Statistical Inference
- Discrete Structures
- Linear Algebra
- Differential Equations

## Authors and acknowledgment

Justin Barsketis - Developer

Micro Economics - Based on Econ 4010 - University of Utah - Professor Lozada
www.economics.utah.edu/lozada

Probability and Statistical Inference for Economists - Based on Econ 3640 - University of Utah - Dr. Haimanti Bhattacharya

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
