import React from 'react'
import { ChartFrequencyDistribution, RenderYoutube } from '../../hooks'
import data from './data.json'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism'
import {
  Heading,
  Text,
  List,
  ListItem,
  OrderedList,
  UnorderedList,
  AspectRatio
} from '@chakra-ui/react'

// https://www.kindacode.com/article/displaying-math-symbols-in-react/
//https://www.npmjs.com/package/react-syntax-highlighter
const module1 = () => {
  const renderExample1 = () => {
    console.log(data['example-1-days'])
    const codeString = `const N = f.reduce((previous, current) => previous + current)
    <Tbody>
    {x.map((item, index) => {
      return (
        <Tr>
          <Td isNumeric>{x[index]}</Td>
          <Td isNumeric>{f[index]}</Td>
          <Td isNumeric>{f[index] / N}</Td>
          <Td isNumeric>{(f[index] / N) * 100}</Td>
        </Tr>
      )
    })}
  </Tbody>`
    return (
      <>
        <Heading as="h2">Chart Frequency Distribution</Heading>
        <ChartFrequencyDistribution
          x={data['example-1-days']}
          f={data['example-1-hours']}
        />
        <SyntaxHighlighter language="javascript" style={dark}>
          {codeString}
        </SyntaxHighlighter>
      </>
    )
  }
  const renderExample2 = () => {
    // 1. Calculate the Range Max - Min / Sub Intervals
    // 2.
    return (
      <>
        <Heading as="h2">Chart Group Frequency Distribution</Heading>
        <OrderedList>
          <ListItem>
            Calculate Bins = Range of Data = Max - Min / Sub Interval. I chose
            10 intervals in this example
          </ListItem>
          <ListItem>
            Iterate through data set, count frequency of each item within range
          </ListItem>
          <ListItem>Graph Results</ListItem>
        </OrderedList>
        <ChartFrequencyDistribution
          x={data['example-1-days']}
          f={data['example-1-hours']}
        />
      </>
    )
  }
  const vocabTerms = () => {
    return (
      <div>
        <UnorderedList>
          <ListItem>
            <Heading as="h2" size="md">
              Graphs for Population Distributions
            </Heading>
            <Heading as="h3" size="sm">
              Histogram
            </Heading>
            <Text>
              Draw a bar above each x value so that the y value corresponds to
              the frequency for that category
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Bar Graph
            </Heading>
            <Text>
              Like a histogram but scores are measured on a nominal or ordinal
              scale, i.e. buckets
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Relative Frequencies
            </Heading>
            <Text>
              As absolute frequencies in a population are hard to assess,
              relative scores are used. height of bars are relative frequencies
              rather than the exact difference
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Smooth Curve
            </Heading>
            <Text>
              The relative change that occurs from one score to the next, to
              show a smooth line or a continious map from point to point.
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h2" size="md">
              Characteristics of a Distribution
            </Heading>
            <Heading as="h3" size="sm">
              Shape
            </Heading>
            <Text>
              Describe the distribution or pattern of the data. Typically
              defined by an equation that shows exact relationship between x and
              y on a graph.
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Central Tendency
            </Heading>
            <Text>
              A single value that attempts to find the central position of the
              data.
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Variability
            </Heading>
            <Text>Shows how spread scores are in a distirbution of data.</Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Categorical: Qualitative Data
            </Heading>
            <Text>
              Qualitative data is defined as the data that approximates and
              characterizes. Qualitative data can be observed and recorded, data
              describes qualities or characteristics. . Color, Blood Types, etc
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Grouped: Quantitative Data
            </Heading>
            <Text>Grouped Data</Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Modal Class
            </Heading>
            <Text>The class that has the most data, or the mode.</Text>
          </ListItem>
          <ListItem>
            <Heading as="h2" size="sm">
              Shape of a Frequency Distribution
            </Heading>
            <Heading as="h3" size="sm">
              Symmetric
            </Heading>
            <Text>
              If a line were to be drawn down the middle, both sides would
              mirror one another. Also the Mean mdian and mode all occur at the
              same point
            </Text>
          </ListItem>
          <ListItem>
            <Heading as="h3" size="sm">
              Skewed
            </Heading>
            <Text>
              Asymmetrical data in which data poinst are grouped together at one
              end. No evenly or normally distributed.
            </Text>
          </ListItem>
        </UnorderedList>
      </div>
    )
  }
  const myUnderstanding = () => {
    return (
      <div>
        <Heading>Frequency Distributions</Heading>
        <Text>
          Definition - In statistics, a frequency distribution is a list, table
          or graph that displays the frequency of various outcomes in a sample.
          Each entry in the table contains the frequency or count of the
          occurrences of values within a particular group or interval.
        </Text>
        <Text>
          My Understanding - Frequency distributions is how often something is
          happening, or how it is the data spread out across the spectrum of
          values. The goal is to find how often data is occuring in each class
          limit, i.e. a group of data or buckets of data.
        </Text>
      </div>
    )
  }
  const helpfulVideos = () => {
    return (
      <>
        <RenderYoutube url={'https://www.youtube.com/embed/T7KYO76DoOE'} />
        <RenderYoutube url={'https://www.youtube.com/embed/ukgdDAcIdUE'} />
      </>
    )
  }
  return (
    <div>
      {myUnderstanding()}
      {helpfulVideos()}
      {vocabTerms()}
      {renderExample1()}
      {renderExample2()}
    </div>
  )
}

export default module1

// Need to finish group frequencies
// Take in Classes, data
// Add state to be able to update the chart
// add error handling try catches to prevent idiots from breaking it
// add youtube embeds https://www.youtube.com/watch?v=ukgdDAcIdUE&ab_channel=mattemath
// chart bar graph, chart histogram, chart smooth curve
