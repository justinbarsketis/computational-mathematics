import logo from './logo.svg'
import './App.css'
import Header from './components/Header'
import Module1 from './components/module1/index'

function App() {
  return (
    <div className="App">
      <Header />
      <Module1 />
    </div>
  )
}

export default App
