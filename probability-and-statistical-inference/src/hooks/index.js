import React from 'react'

import MathJax from 'react-mathjax'
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  AspectRatio,
  Center,
  Flex
} from '@chakra-ui/react'
// https://www.kindacode.com/article/displaying-math-symbols-in-react/

export const ChartFrequencyDistribution = ({ x, f }) => {
  const N = f.reduce((previous, current) => previous + current)
  return (
    <Table variant="simple">
      <TableCaption>Hours a person watches of television per day</TableCaption>
      <Thead>
        <MathJax.Provider>
          <Tr>
            <Th isNumeric>
              <MathJax.Node inline formula={`X`} />
            </Th>
            <Th isNumeric>
              <MathJax.Node inline formula={`F`} />
            </Th>
            <Th isNumeric>
              <MathJax.Node inline formula={`\\rho\\ = f/N`} />
            </Th>
            <Th isNumeric>
              <MathJax.Node inline formula={`\\rho\\ * 100`} />
            </Th>
          </Tr>
        </MathJax.Provider>
      </Thead>
      <Tbody>
        {x.map((item, index) => {
          return (
            <Tr>
              <Td isNumeric>{x[index]}</Td>
              <Td isNumeric>{f[index]}</Td>
              <Td isNumeric>{f[index] / N}</Td>
              <Td isNumeric>{(f[index] / N) * 100}</Td>
            </Tr>
          )
        })}
      </Tbody>
    </Table>
  )
}

export const ChartGroupFrequencyDistribution = ({ x, groupSize }) => {
  // const N = f.reduce((previous, current) => previous + current)
  const range = Math.range(x)
  console.log(range)

  return (
    <Table variant="simple">
      <TableCaption>Hours a person watches of television per day</TableCaption>
      <Thead>
        <MathJax.Provider>
          <Tr>
            <Th isNumeric>
              <MathJax.Node inline formula={`X`} />
            </Th>
            <Th isNumeric>
              <MathJax.Node inline formula={`F`} />
            </Th>
          </Tr>
        </MathJax.Provider>
      </Thead>
      {/* <Tbody>
        {x.map((item, index) => {
          return (
            <Tr>
              <Td isNumeric>{x[index]}</Td>
              <Td isNumeric>{f[index]}</Td>
              <Td isNumeric>{f[index] / N}</Td>
              <Td isNumeric>{(f[index] / N) * 100}</Td>
            </Tr>
          )
        })}
      </Tbody> */}
    </Table>
  )
}

export const RenderYoutube = ({ url }) => {
  return (
    <Flex>
      <Center flex="1 1 auto">
        <AspectRatio minW="560px" maxW="560px" ratio={16 / 9}>
          <iframe title="Frequency Distributions" src={url} allowFullScreen />
        </AspectRatio>
      </Center>
    </Flex>
  )
}
