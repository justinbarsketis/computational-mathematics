# Basic Mathematics

0. Basic Supply and Demand Graphs
1. Slopes
2. Graph of Slopes
3. Averages
4. Averages and Marginals
5. Income Tax Example
6. Social Security Tax Example
7. Medicare Premium Example
8. Examples inspired by the Theory of the Film
9. Inverse Marginal
10. Inverse Average
11. Contour Lines Part 1
12. Countour Lines Part 2
13. Convex and Concave Functions

## Installation

To be determined, I will most liklely use python for all of these projects.

## Usage

This code is to be used for learning purposes.

## Support

Email justinbarsketis@gmail.com

## Authors and acknowledgment

Justin Barsketis

Micro Economics - Based on Econ 4010 - Advanced Micro Economics - University of Utah - Professor Lozada
www.economics.utah.edu/lozada

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
