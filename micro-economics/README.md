# Micro Economics

In this repo I will create components that help calculate and graph common micro economic principles

## Installation

To be determined, I will most liklely use python for all of these projects.

## Usage

This code is to be used for learning purposes only or reference material.

## Support

Email justinbarsketis@gmail.com

## Roadmap

- Basic Mathematics
- Theory of Choice
- Changes in Income and Prices
- Market Demand and Elasticity
- The Technology of Production
- Cost Functions
- Profit
- Competitive Equilibrium
- Tax Incidence
- Monopoly
- Consumer and Producer Surplus
- Input Markets
- Dynamic Economics
- The Edgeworth Box

## Authors and acknowledgment

Justin Barsketis

Micro Economics - Based on Econ 4010 - University of Utah - Professor Lozada
www.economics.utah.edu/lozada

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
